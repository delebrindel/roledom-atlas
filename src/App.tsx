import { ItemFinder } from "./components/ItemFinder/ItemFinder"

function App() {

  return (
    <div className="App">
      <ItemFinder />
    </div>
  )
}

export default App
