import { FC, useState } from "react";
import { ITEM_TYPES } from "../../enums/Generic";
import { BuildingList } from "./BuildingList";
import { GemList } from "./GemList";

const ITEM_KEYS = Object.keys(ITEM_TYPES);

export const ItemFinder: FC = () => {
  const [searchItem, setSearchItem] = useState("");
  const [itemDisplayed, setItemDisplayed] = useState<string>(ITEM_TYPES.GEMS);

  const changeQuery = (e: React.FormEvent<HTMLInputElement>) => {
    setSearchItem(e.currentTarget.value);
  };

  return (
    <section>
      <h2>Buscar Objeto</h2>
      <ul className="optionList">
        {ITEM_KEYS.map((itemKey: string, itemKeyIndex) => (
          <li
            className={itemDisplayed === ITEM_TYPES[itemKey] ? "active" : ""}
            key={`it-${itemKeyIndex}`}
            onClick={()=>{setItemDisplayed(ITEM_TYPES[itemKey])}}
          >
            {ITEM_TYPES[itemKey]}
          </li>
        ))}
      </ul>
      <label htmlFor="query">Objeto Buscado</label>
      <input
        type="text"
        name="query"
        value={searchItem}
        onChange={changeQuery}
      />
      <hr />
      {itemDisplayed === ITEM_TYPES.BUILDINGS && <BuildingList query={searchItem} />}
      {itemDisplayed === ITEM_TYPES.GEMS && <GemList query={searchItem} />}
    </section>
  );
};
