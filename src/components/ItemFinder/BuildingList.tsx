import { FC, useState } from "react";
import { BuildingItem, Buildings } from "../../enums/Buildings";
import { ITEM_TYPES, MESSAGES } from "../../enums/Generic";
import { useDebouncedEffect } from "../../hooks/use-debounced-effect";
import { filterByQuery } from "../../utils/Filters";
import { Building } from "./Item";

type ItemListProps = {
  query: string;
};

const BUILDING_LIST: BuildingItem[] = [...Buildings];

export const BuildingList: FC<ItemListProps> = ({ query }) => {
  const [gems, setGems] = useState([]);
  const [isFiltering, setIsFiltering] = useState(false);

  useDebouncedEffect(
    () => {
      !isFiltering && setIsFiltering(true);
      let itemList = [];
      if (BUILDING_LIST && query !== "") {
        itemList = filterByQuery(BUILDING_LIST, query, ["description", "name"]);
      } else {
        itemList = BUILDING_LIST;
      }
      setGems(itemList);
      setTimeout(() => {
        setIsFiltering(false);
      }, 200);
    },
    [query],
    250
  );

  return (
    <section>
      <h3>{ITEM_TYPES.BUILDINGS}</h3>
      <p>
        Resultados para ' <strong>{query}</strong> '
      </p>
      {gems.length > 0 ? (
        <>
          <div className="itemListWrapper">
            <article className="flexRow title">
              <div className="g_20">Nombre</div>
              <div className="g_50">Descripción</div>
              <div className="g_30">Precio</div>
            </article>
            {gems.map((gemItem, index) => (
              <Building key={`g${index}`} building={gemItem} />
            ))}
          </div>
        </>
      ) : (
        <h3>{MESSAGES.NO_ITEMS_FOUND}</h3>
      )}
    </section>
  );
};
