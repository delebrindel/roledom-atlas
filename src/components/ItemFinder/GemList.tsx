import { FC, useState } from "react";
import { GemItem, Gems } from "../../enums/Gems";
import { ITEM_TYPES, MESSAGES } from "../../enums/Generic";
import { useDebouncedEffect } from "../../hooks/use-debounced-effect";
import { filterByQuery } from "../../utils/Filters";
import { Gem } from "./Item";

type ItemListProps = {
  query: string;
};

const GEM_LIST: GemItem[] = [...Gems];

export const GemList: FC<ItemListProps> = ({ query }) => {
  const [gems, setGems] = useState([]);
  const [isFiltering, setIsFiltering] = useState(false);

  useDebouncedEffect(
    () => {
      !isFiltering && setIsFiltering(true);
      let itemList = [];
      if (GEM_LIST && query !== "") {
        itemList = filterByQuery(GEM_LIST, query, ['variety', 'size']);
      } else {
        itemList = GEM_LIST;
      }
      setGems(itemList);
      setTimeout(() => {
        setIsFiltering(false);
      }, 200);
    },
    [query],
    250
  );

  return (
    <section>
    <h3>{ITEM_TYPES.GEMS}</h3>
      <p>
        Resultados para ' <strong>{query}</strong> '
      </p>
      {gems.length > 0 ? (
        <>
          <div className="itemListWrapper">
            <article className='flexRow title'>
              <div className="g_70">Nombre</div>
              <div className="g_30">Precio</div>
            </article>
            {gems.map((gemItem, index) => <Gem key={`g${index}`}  gem={gemItem}/>)}
          </div>
        </>
      ) : (
        <h3>{MESSAGES.NO_ITEMS_FOUND}</h3>
      )}
    </section>
  );
};
