import { FC } from "react";
import { FormatAmount } from "tresutils";
import { BuildingItem } from "../../enums/Buildings";
import { GemItem } from "../../enums/Gems";


type GemProps = {
  gem: GemItem;
}

type BuildingProps = {
  building: BuildingItem;
}

export const Gem: FC<GemProps> = ({gem})=>{
  return <article className='flexRow striped'>
    <div className='g_70'>{`${gem.variety} (${gem.size})`}</div>
    <div className='g_30'>{FormatAmount(gem.goldValue)}</div>
  </article>
}

export const Building: FC<BuildingProps> = ({building})=>{
  return <article className='flexRow striped'>
    <div className='g_20'>{building.name}</div>
    <div className='g_50'>{building.description}</div>
    <div className='g_30'>{FormatAmount(building.goldValue)}</div>
  </article>
}