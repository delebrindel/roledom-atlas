enum GEM_SIZES {
  SMALL = "Peq",
  MEDIUM = "Med",
  LARGE = "Gde",
  GIANT = "Gig",
}

enum GEM_NAMES {
  AQUAMARINE = "Acuamarina",
  HEMATITE = "Hematita",
  LAPIZ_LAZULI = "Lapis Lazuli",
  OBSIDIAN = "Obsidiana",
  RIVER_PEARL = "Perla de río",
  EMERALD = "Esmeralda",
  OPAL = "Ópalo",
  SAPPHIRE = "Zafiro",
  RUBY = "Rubí",
}

export type GemItem = {
  variety: GEM_NAMES;
  size: GEM_SIZES;
  goldValue: number;
};

export const Gems: GemItem[] = [
  { variety: GEM_NAMES.AQUAMARINE, size: GEM_SIZES.SMALL, goldValue: 50 },
  { variety: GEM_NAMES.AQUAMARINE, size: GEM_SIZES.MEDIUM, goldValue: 200 },
  { variety: GEM_NAMES.AQUAMARINE, size: GEM_SIZES.LARGE, goldValue: 700 },
  { variety: GEM_NAMES.EMERALD, size: GEM_SIZES.SMALL, goldValue: 120 },
  { variety: GEM_NAMES.EMERALD, size: GEM_SIZES.MEDIUM, goldValue: 520 },
  { variety: GEM_NAMES.EMERALD, size: GEM_SIZES.LARGE, goldValue: 1600 },
  { variety: GEM_NAMES.HEMATITE, size: GEM_SIZES.SMALL, goldValue: 30 },
  { variety: GEM_NAMES.HEMATITE, size: GEM_SIZES.MEDIUM, goldValue: 120 },
  { variety: GEM_NAMES.HEMATITE, size: GEM_SIZES.LARGE, goldValue: 400 },
  { variety: GEM_NAMES.LAPIZ_LAZULI, size: GEM_SIZES.SMALL, goldValue: 25 },
  { variety: GEM_NAMES.LAPIZ_LAZULI, size: GEM_SIZES.MEDIUM, goldValue: 95 },
  { variety: GEM_NAMES.LAPIZ_LAZULI, size: GEM_SIZES.LARGE, goldValue: 300 },
  { variety: GEM_NAMES.OBSIDIAN, size: GEM_SIZES.SMALL, goldValue: 45 },
  { variety: GEM_NAMES.OBSIDIAN, size: GEM_SIZES.MEDIUM, goldValue: 180 },
  { variety: GEM_NAMES.OBSIDIAN, size: GEM_SIZES.LARGE, goldValue: 600 },
  { variety: GEM_NAMES.OPAL, size: GEM_SIZES.SMALL, goldValue: 115 },
  { variety: GEM_NAMES.OPAL, size: GEM_SIZES.MEDIUM, goldValue: 380 },
  { variety: GEM_NAMES.OPAL, size: GEM_SIZES.LARGE, goldValue: 950 },
  { variety: GEM_NAMES.RIVER_PEARL, size: GEM_SIZES.SMALL, goldValue: 15 },
  { variety: GEM_NAMES.RIVER_PEARL, size: GEM_SIZES.MEDIUM, goldValue: 60 },
  { variety: GEM_NAMES.RIVER_PEARL, size: GEM_SIZES.LARGE, goldValue: 200 },
  { variety: GEM_NAMES.RUBY, size: GEM_SIZES.SMALL, goldValue: 180 },
  { variety: GEM_NAMES.RUBY, size: GEM_SIZES.MEDIUM, goldValue: 620 },
  { variety: GEM_NAMES.RUBY, size: GEM_SIZES.LARGE, goldValue: 1900 },
  { variety: GEM_NAMES.SAPPHIRE, size: GEM_SIZES.SMALL, goldValue: 150 },
  { variety: GEM_NAMES.SAPPHIRE, size: GEM_SIZES.MEDIUM, goldValue: 590 },
  { variety: GEM_NAMES.SAPPHIRE, size: GEM_SIZES.LARGE, goldValue: 1750 },
];
