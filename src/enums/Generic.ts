export enum MESSAGES {
  NO_ITEMS_FOUND = 'No se encontraron objetos que cumplieran con los criterios de búsqueda especificados.'
}

interface DataMap {
  [key: string]: string;
}

export const ITEM_TYPES: DataMap = {
  "BUILDINGS": 'Edificaciones',
  "GEMS":  'Gemas',
}