export type BuildingItem = {
  name: string;
  description: string;
  goldValue: number;
};

export const Buildings: BuildingItem[] = [
  { name: 'Castillo', description: 'Alberga a miles de personas, tiene una muralla y un torreón.', goldValue: 250000 },
];
