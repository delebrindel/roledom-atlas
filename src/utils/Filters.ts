export const filterByQuery = (list: any, query: string, searchableFields: string[]) => {
  query = String(query).toLowerCase();
  if (query !== '' && query) {
    let filteredList: any[] = [];
    list.forEach((listItem: any) => {
      let hasValue = false;
      searchableFields.forEach((searchableField) => {
        if (String(listItem[searchableField]).toLowerCase().includes(query)) hasValue = true;
      });
      hasValue && filteredList.push(listItem);
    });
    return filteredList;
  }
  return list;
};